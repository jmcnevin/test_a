# frozen_string_literal: true

require 'rubygems'
require 'bundler/setup'

Bundler.require(:default)

Dir[File.join(File.dirname(__FILE__), 'lib', 'next_bus', '*.rb')].each do |file|
  require file
end

NextBus::CLI.new.start
