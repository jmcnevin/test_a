# frozen_string_literal: true
require 'test_helper'
require_relative '../../lib/next_bus/metro_transit_client'

class TestMetroTransitClient < Minitest::Test
  def setup
    @client = NextBus::MetroTransitClient.new
  end

  def test_routes
    VCR.use_cassette('routes') do
      result = @client.routes
      assert result.is_a?(Array)
      assert_equal 223, result.size
      assert_equal result.first,
                   'Description' => 'METRO Blue Line',
                   'ProviderID' => '8',
                   'Route' => '901'
    end
  end

  def test_find_routes
    VCR.use_cassette('routes') do
      result = @client.find_routes('Blue')
      assert_equal result.first,
                   'Description' => 'METRO Blue Line',
                   'ProviderID' => '8',
                   'Route' => '901'
    end
  end

  def test_directions
    VCR.use_cassette('directions') do
      result = @client.directions(901)
      assert_equal [4, 1], result
    end
  end

  def test_stops
    VCR.use_cassette('stops') do
      result = @client.stops(901, 1)
      assert result.is_a?(Array)
      assert_equal result.first,
                   'Text' => 'Target Field Station Platform 2',
                   'Value' => 'TF22'
    end
  end

  def test_find_stops
    VCR.use_cassette('stops') do
      result = @client.find_stops(901, 1, 'Platform 2')
      assert_equal result.first,
                   'Text' => 'Target Field Station Platform 2',
                   'Value' => 'TF22'
    end
  end

  def test_departures
    VCR.use_cassette('departures') do
      result = @client.departures(901, 1, 'TF22')
      assert result.is_a?(Array)
      assert_equal result.first,
                   'Actual' => false,
                   'BlockNumber' => 1,
                   'DepartureText' => '9:06',
                   'DepartureTime' => '/Date(1481555160000-0600)/',
                   'Description' => 'to Mall of America',
                   'Gate' => '2',
                   'Route' => 'Blue',
                   'RouteDirection' => 'SOUTHBOUND',
                   'Terminal' => '',
                   'VehicleHeading' => 0,
                   'VehicleLatitude' => 0,
                   'VehicleLongitude' => 0
    end
  end

  def test_next_departure
    VCR.use_cassette('departures') do
      result = @client.next_departure(901, 1, 'TF22')
      assert_equal result, '9:06'
    end
  end
end
