Test A
======

Requirements
------------

* Ruby >= 2.0
* Git

Setup
-----

```
git clone git@gitlab.com:jmcnevin/test_a.git
cd test_a
gem install bundler --no-ri --no-rdoc
bundle
```

Running
-------

```
ruby next_bus.rb ROUTE_NAME DIRECTION STOP_NAME
```

Example
-------

```
ruby next_bus.rb "METRO Blue Line" "Target Field Station Platform 1" "south"
```


Testing
-------

To run tests, start the listener:

```
guard start
```

(this command will also start the YARD documentation server at localhost:8808)

