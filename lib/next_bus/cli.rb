# frozen_string_literal: true

require_relative 'debugger'

module NextBus
  class CLI
    include Debugger

    def initialize
      @highline = ::HighLine.new
      @client = MetroTransitClient.new
    end

    def start
      route = get_route
      direction = get_direction(route)
      stop = get_stop(route, direction)
      print_result(route, direction, stop)
    rescue MetroTransitClient::ClientError => e
      puts 'Sorry, an error occurred.'
      puts e.message
    end

    private

    def get_route
      route = ARGV[0]
      routes = route ? @client.find_routes(route) : @client.routes
      return routes.first.fetch('Route') if routes.size == 1
      selected =
        @highline.choose do |menu|
          menu.prompt = 'Select route:  '
          routes.each do |r|
            menu.choice("#{r.fetch('Description')} (#{r.fetch('Route')})")
          end
        end
      /\((?<route_number>[0-9]+)\)\z/ =~ selected
      route_number
    end

    def get_direction(route)
      direction = ARGV[2]
      return @client.direction_id(direction) if direction
      directions = @client.directions(route)
      selected =
        @highline.choose do |menu|
          menu.prompt = 'Select direction:  '
          directions.each do |d|
            menu.choice(@client.direction(d))
          end
        end
      @client.direction_id(selected)
    end

    def get_stop(route, direction)
      stop = ARGV[1]
      stops = stop ? @client.find_stops(route, direction, stop) : @client.stops(route, direction)
      return stops.first.fetch('Value') if stops.size == 1
      selected =
        @highline.choose do |menu|
          menu.prompt = 'Select stop:  '
          stops.each do |s|
            menu.choice("#{s.fetch('Text')} (#{s.fetch('Value')})")
          end
        end
      /\((?<stop_name>[A-Z]+)\)\z/ =~ selected
      stop_name
    end

    def print_result(route, direction, stop)
      result = @client.next_departure(route, direction, stop)
      @highline.say "Next departure: #{result}"
    end
  end
end
