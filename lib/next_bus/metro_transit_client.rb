# frozen_string_literal: true

require 'uri'
require 'json'
require_relative 'debugger'

module NextBus
  class MetroTransitClient
    include Debugger

    URI_ROOT = 'http://svc.metrotransit.org/NexTrip/'.freeze
    DIRECTIONS = %w(north east west south).freeze # do not change order

    ClientError = Class.new(StandardError)
    NoMatchesFoundError = Class.new(ClientError)

    # @return [Array<Hash>] data about available routes
    def routes
      @routes ||= get('Routes')
    end

    # @param text [String] route name or substring
    # @return [Array<Hash>] data about the matching routes
    def find_routes(text)
      find_entities(routes, text, 'Description')
    end

    # @param route_id [#to_i] unique ID of route
    # @return [Array<Integer>] integer representation of available route directions
    def directions(route_id)
      @directions ||= Hash.new do |hash, key|
        hash[key] = get('Directions', key).collect { |x| x['Value'].to_i }
      end

      # caching response
      @directions[route_id]
    end

    # @param route_id [#to_i] unique ID of route
    # @param direction_id [#to_i] direction ID
    # @return [Array<Hash>] data about stops along this route
    def stops(route_id, direction_id)
      @stops ||= Hash.new do |hash, key|
        hash[key] = get('Stops', *key)
      end

      # caching response
      @stops[[route_id, direction_id]]
    end

    # @param route_id [#to_i] unique ID of route
    # @param direction_id [#to_i] direction ID
    # @param text [String] stop name or substring
    # @return [Array<Hash>] data about the matching stops
    def find_stops(route_id, direction_id, text)
      arr = stops(route_id, direction_id)
      find_entities(arr, text)
    end

    # @param route_id [#to_i] unique ID of route
    # @param direction_id [#to_i] direction ID
    # @param stop_id [String] unique ID of stop
    # @return [Array<Hash>] data about future departures
    def departures(route_id, direction_id, stop_id)
      get(route_id, direction_id, stop_id)
    end

    # @param (see #departures)
    # @return [String] text describing when the next bus will arrive
    def next_departure(*args)
      result = departures(*args).first
      result = result.fetch('DepartureText') if result.is_a?(Hash)
      result || 'none'
    end

    # @param direction [String] a named direction
    # @return [Integer] direction ID
    def direction_id(direction)
      DIRECTIONS.index(direction.to_s.downcase) + 1
    end

    # @param direction_id [Integer] direction ID
    # @return [String] a named direction
    def direction(direction_id)
      DIRECTIONS[direction_id.to_i - 1]
    end

    private

    def find_entities(collection, text, attr_name = 'Text')
      matches = collection.select { |x| x[attr_name].include?(text) }
      debug { matches }
      if matches.empty?
        raise NoMatchesFoundError, %(Nothing found matching "#{text}.")
      end
      matches
    end

    def uri(*fragments)
      fragments = fragments.collect { |x| escape(x) }
      path = File.join(fragments).gsub(%r{\A/}, '')
      URI.join(URI_ROOT, path)
    end

    def escape(str)
      URI.escape(str.to_s)
    end

    def get(*fragments)
      url = uri(*fragments).to_s
      debug { "URL: #{url}" }
      response = ::RestClient.get(url, accept: :json)
      debug { "CODE: #{response.code}" }
      debug { "BODY: #{response.body}" }
      JSON.parse(response.body)
    end
  end
end
