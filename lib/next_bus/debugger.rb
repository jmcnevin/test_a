# frozen_string_literal: true
module NextBus
  module Debugger
    private

    def debug
      return unless ENV['DEBUG'].to_s == '1'
      x = yield
      puts case x
           when String then x
           else x.inspect
           end
    end
  end
end
